// Get the cookie out of the cookie store
const payloadCookie = await cookieStore.get('jwt_access_payload')
if (payloadCookie) {
    console.log("got the cookie")
    // The cookie value is a JSON-formatted string, so parse it
    const encodedPayload = payloadCookie.value
    // console.log("the encoded payload cookie", encodedPayload)


    // Convert the encoded payload from base64 to normal string
    const decodedPayload = atob(encodedPayload)
    // console.log ("the decoded payload cookie", decodedPayload)


    // The payload is a JSON-formatted string, so parse it
    const payload = JSON.parse(decodedPayload)

    // Print the payload
    // console.log(payload);

    if (payload.user.perms.includes("events.add_conference")) {
        let newConferenceLink = document.getElementById("new-conference-link")
        newConferenceLink.classList.remove('d-none');
    };

    if (payload.user.perms.includes("events.add_location")){
        let newLocationLink = document.getElementById("new-location-link")
        newLocationLink.classList.remove('d-none');
    }
    // Check if "events.add_conference" is in the permissions.
    // If it is, remove 'd-none' from the link


    // Check if "events.add_location" is in the permissions.
    // If it is, remove 'd-none' from the link

} else {
    console.log("that didn't work");
}
